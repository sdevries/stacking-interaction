"""
Experiment to see how fast a polyA chain would grow, starting from a single conformer.
Only consider the dinucleotide primary 1A library, and explore only growings with an overlap RMSD of <1A.
For the initial conformer, don't choose one, but assume a probabilistic distribution proportional to the cluster size.
"""

import numpy as np
from tqdm import tqdm
from clusterlib import read_clustering
from nefertiti.functions.superimpose import superimpose_array

precision = 1.0
coor = np.load(f"library/dinuc-AA-{precision}.npy")
clustering = read_clustering(f"lib-dinuc-AA-{precision}.clust")
coor_indices = np.loadtxt(
    f"build-library-dinuc-AA-{precision}-primary-indices.txt"
).astype(int)

# probability vector
ind_to_cluster = {}
for clusnr, clus in enumerate(clustering):
    for ind in clus:
        ind_to_cluster[ind] = clusnr
assert len(coor_indices) == len(coor)
coor_clusters = [ind_to_cluster[ind] for ind in coor_indices]
cluster_lengths = np.array([len(clustering[c]) for c in coor_clusters])
prob_vector = cluster_lengths / cluster_lengths.sum()

# overlap RMSD
sizeA = 22
###assert 2 * sizeA == coor.shape[1]
overlap_zone = coor.shape[1] - sizeA
pre = coor[:, -overlap_zone:]
post = coor[:, :overlap_zone]
rmsd = np.empty((len(coor), len(coor)))
for n in tqdm(list(range(len(coor)))):
    r = superimpose_array(pre, post[n])[1]
    rmsd[:, n] = r

transmat = (rmsd < precision).astype(bool)

dist = prob_vector
print(-1, np.log10(dist.sum()), dist[:5], prob_vector[dist.argmax()])
for n in range(5):
    new_dist = (dist[:, None] * transmat).sum(axis=0)
    print(
        n,
        np.log10(new_dist.sum()),
        new_dist[:5] / new_dist.sum(),
    )
    dist = new_dist

# This will produce chains of conf1-conf2-conf3-...
# conf1 is the probabilistic one (#chains for conf1  = 1)
# What is the branching factor?
# answer: 660 (10**2.82) for conf1-conf2 chains, then x400 (10**2.6) for each additional dinuc.
# This is comparable to the mean of the 1A transition matrix, which is 422

# NOTE: changing the precision to 0.5 increases the branching factor to 2500-3000 for overlap RMSD of 1A...
# However, if the overlap RMSD is then *also* changed to 0.5, the branching is similar as before (631 for step 1, then 316 )

# for trinuc, 1A: 200 for step 1, 32 after.
# for trinuc, 0.5A: 120 for step 1, 40 after.
