for MMCIF headers:

polyribonucleotide
["entity_poly"]["type"]:
RNA: 'polyribonucleotide'
DNA: 'polydeoxyribonucleotide'
protein: 'polypeptide(L)'

There is more exotic stuff such as DNA/RNA hybrid, which we can ignore.

Initial conclusion
Single-ring stacking interactions (PHE/TYR - U/C)

In translational space: 886 grid points that fulfill the criteria
Note that in the typical case, half of them can be discarded immediately because
they are on the wrong side (above/below) of the ring.

In rotational space: 8.3 % of plane-plane orientations is kept. 
In addition, 75 % of dihedral space is kept, for a total of 6.2 % of rotational space.

The trinucleotide library has typically 40k rotamers per conformer. 
6.2 % of rotational space is about 2.5k rotamers per conformer kept.

With 4k conformers per library, this is 10 million rotaconformers, 
 or about 9 billion potential docking solutions.

For double stacking:
A test has been done in the 1B7F-stack folder in a different project
The plane angle reduction is independent, i.e. 
  8.3 % of 8.3 % of 40k = 275 rotamers per conformer
In fact, in the 1B7F test, this is confirmed,
 as ~220 rotamers per conformer, or 800k rotaconformers in total.
The dihedral filter (75 % per stacking, or 56 % for two stackings) is also 
 approx. independent, selecting 450k rotaconformers in total.

pseudo-crocodile: this leads to approximately 19 million poses.
However, distance criteria need to be relaxed by 0.5 to account for grid spacing, bringing
the number of poses to 130 million. However, the best RMSD is 0.43 A!

NOTE: Instead of filtering at sin(angle)<0.4, one might consider a stricter cutoff.
For example, angle<15 degrees corresponds only to 3.4 % instead of 8.3 % 
 of the rotamers, a factor 2.44. For the double stacking, the factor would be squared
 too, i.e. an factor 6 in reduction of conformational space.
However, one must consider that filtering at 15 degrees only covers 62.9 % of stackings instead of 94.6 %.
In fact, for the 1B7F test, all rotaconformers are lost!