import numpy as np
from seamless import Buffer, Checksum, transformer
from tqdm import tqdm
import center_vector

import seamless
seamless.delegate(level=3)

@transformer(return_transformation=True)
def center_vector_chunk(struc_chunk, interface_chunk_index, interface_chunk_data, prot_resnames, na_resnames, max_dis, max_sin_angle):
    import numpy as np
    from .center_vector import center_vector
    results = []
    for code, struc0 in struc_chunk.items():
        if_start, if_size = interface_chunk_index[code]
        if not if_size:
            continue
        interfaces = interface_chunk_data[if_start:if_start+if_size]
        models = np.unique(struc0["model"])
        for modelnr in models:
            struc = struc0[struc0["model"] == modelnr]
            result0 = center_vector(struc, interfaces, prot_resnames, na_resnames, max_dis, max_sin_angle)
            for ringring, result in result0:
                if len(result):
                    results.append(result)
    if not len(results):
        return np.array([])
    
    results = np.concatenate(results)
    return results
center_vector_chunk.celltypes.struc_chunk = "mixed"
center_vector_chunk.celltypes.interface_chunk_index = "plain"
center_vector_chunk.celltypes.interface_chunk_data = "binary"
center_vector_chunk.celltypes.prot_resnames = "plain"
center_vector_chunk.celltypes.na_resnames = "plain"
center_vector_chunk.celltypes.max_dis = "float"
center_vector_chunk.celltypes.max_sin_angle = "float"
center_vector_chunk.modules.center_vector = center_vector

interface_index, interface_data = Buffer.load("allpdb-filtered-interfaces").deserialize("mixed")
struc = Buffer.load("allpdb-interface-struc").deserialize("mixed")
prot_resnames = (("PHE", "TYR"))
na_resnames = (("U", "C"))
max_dis = 5.3
max_sin_angle = 0.4

allpdb_keyorder = Checksum.load("allpdb-keyorder.CHECKSUM")
allpdb_keyorder = allpdb_keyorder.resolve("plain")

all_keys = []
for key in allpdb_keyorder:
    if key in interface_index:
        all_keys.append(key)

chunksize = 10
key_chunks = [all_keys[n:n+chunksize] for n in range(0, len(all_keys), chunksize)]
nchunks = len(key_chunks)

print("START")

def center_vector_chunk0(chunk_index):
    keys = key_chunks[chunk_index]
    offset = 0
    interface_chunk_index = {}
    interface_chunk_data = []
    struc_chunk = {}
    for code in keys:
        start, nif = interface_index[code]
        ifaces = interface_data[start:start+nif]
        interface_chunk_index[code] = (offset, nif)
        interface_chunk_data.extend(ifaces)
        offset += nif
        struc_chunk[code] = struc[code]
    if not offset:
        return np.array([])
    interface_chunk_data = np.array(interface_chunk_data, dtype=interface_chunk_data[0].dtype)
    return center_vector_chunk(
        struc_chunk, 
        interface_chunk_index, interface_chunk_data, 
        prot_resnames, na_resnames,
        max_dis, max_sin_angle
    )

POOLSIZE = 10
with tqdm(total=nchunks, desc="Center-vector analysis") as progress_bar:
    def callback(n, processed_chunk):
        progress_bar.update(1)
        if processed_chunk.checksum.value is None:
            print(
                f"""Failure for chunk {n}:
    status: {processed_chunk.status}
    exception: {processed_chunk.exception}
    logs: {processed_chunk.logs}"""
            )

    with seamless.multi.TransformationPool(POOLSIZE) as pool:
        processed_chunks = pool.apply(center_vector_chunk0, nchunks, callback=callback)

if not any([processed_chunk.checksum.value is None for processed_chunk in processed_chunks]):
    results_cs = [processed_chunk.checksum for processed_chunk in processed_chunks]
    results = []
    
    '''
    # Naive way
    for cs in tqdm(results_cs, desc="Collect center-vector analysis results..."):
        chunk = cs.resolve("binary")
        results.append(chunk)
    '''
    with tqdm(total=len(processed_chunks), desc="Collect center-vector analysis results...") as progress_bar:
        for chunk in seamless.multi.resolve(results_cs, nparallel=50, celltype="binary", callback= lambda *_: progress_bar.update(1)):
            if len(chunk):
                results.append(chunk)

    results = np.concatenate(results, dtype=results[0].dtype)
    print("...done")


buf = Buffer(results, celltype="binary")
buf.save("center-vector-single-ring")
buf.checksum.save("center-vector-single-ring.CHECKSUM")