import itertools
import os
import numpy as np
import sys
from seamless import Buffer
from trinuc_fit_roco import trinuc_fit_roco
from nefertiti.functions.parse_mmcif import atomic_dtype

def err(msg):
    print(msg, file=sys.stderr)
    exit(1)



chunk = int(sys.argv[1])

rna_strucs = {}
trinuc_fitted = {}
rna_strucs_index, rna_strucs_data = Buffer.load("allpdb-rna-attract").deserialize("mixed")
trinuc_fitted_index, trinuc_fitted_data = Buffer.load(f"allpdb-trinuc-fit-chunk-{chunk+1}").deserialize("mixed")

codes = list(trinuc_fitted_index.keys())
for code in codes:
    pos, size = trinuc_fitted_index[code]
    trinuc_fitted[code] = trinuc_fitted_data[pos:pos+size]
    pos, size = rna_strucs_index[code]
    rna_strucs[code] = rna_strucs_data[pos:pos+size]


bases = ("A", "C", "G", "U")
trinuc_sequences = ["".join(s) for s in itertools.product(bases, repeat=3)]

template_pdbs = {}
for seq in trinuc_sequences:
    filename = f"templates/{seq}-template-ppdb.npy"
    template = np.load(filename)
    if template.dtype != atomic_dtype:
        err(f"Template '{filename}' does not contain a parsed PDB")
    template_pdbs[seq] = template


conformers = {}
for seq in trinuc_sequences:
    filename = f"/home/sjoerd/data/work/ProtNAff/database/trilib/{seq}-lib-conformer.npy"
    conformer = np.load(filename)
    if conformer.dtype not in (np.float32, np.float64):
        err(f"Conformer file '{filename}' does not contain an array of numbers")
    if conformer.ndim != 3:
        err(f"Conformer file '{filename}' does not contain a 3D coordinate array")
    if conformer.shape[1] != len(template_pdbs[seq]):
        err(
            f"Sequence {seq}: conformer '{filename}' doesn't have the same number of atoms as the template"
        )
    conformers[seq] = conformer.astype(float)

rotaconformer_indices = {}
for seq in trinuc_sequences:
    seq2 = seq.replace("U", "C").replace("G", "A")
    if seq == seq2:
        filename = f"/home/sjoerd/data/work/crocodile/make-rotaconformers/results/{seq}-lib-rotaconformer.json"
        rotaconformer_index = Buffer.load(filename).deserialize("plain")
        if not isinstance(rotaconformer_index, list):
            err(
                f"Sequence {seq}: '{filename}' is not a list of filenames/checksums"
            )
        if len(rotaconformer_index) != len(conformers[seq]):
            err(
                f"Sequence {seq}: There are {len(conformers[seq])} conformers but {len(rotaconformer_index)} rotaconformers"
            )
        for fnr, f in list(enumerate(rotaconformer_index)):
            rotaconformer_index[fnr] = os.path.join(
                #"/home/sjoerd/mbi-frontend/data3/sdevries/seamless/buffers", f
                "/data/sjoerd/rotaconformer-buffers", f
            )
        rotaconformer_indices[seq] = rotaconformer_index
for seq in trinuc_sequences:
    seq2 = seq.replace("U", "C").replace("G", "A")
    rotaconformer_indices[seq] = rotaconformer_indices[seq2]

trinuc_rotaconformer_library = {}
for seq in rotaconformer_indices:
    for n,filename in enumerate(rotaconformer_indices[seq]):
        trinuc_rotaconformer_library[seq, n] = filename

result = trinuc_fit_roco(
    trinuc_fitted,
    rna_strucs,
    template_pdbs=template_pdbs,
    trinuc_conformer_library=conformers,    
    trinuc_rotaconformer_library=rotaconformer_indices,
    rmsd_margin2=0.5,
    conformer_rmsd_min=0.1,
    conformer_rmsd_max=1.0,
)

buf = Buffer(result, celltype="mixed")
buf.save(f"allpdb-trinuc-fit-roco-chunk-{chunk}")
