import os
import itertools
import numpy as np
import sys
from seamless import Buffer
from trinuc_roco_grid_overlap_rmsd import trinuc_roco_grid_overlap_rmsd
from nefertiti.functions.parse_mmcif import atomic_dtype

def err(msg):
    print(msg, file=sys.stderr)
    exit(1)

bases = ("A", "C", "G", "U")
trinuc_sequences = ["".join(s) for s in itertools.product(bases, repeat=3)]

template_pdbs = {}
for seq in trinuc_sequences:
    filename = f"templates/{seq}-template-ppdb.npy"
    template = np.load(filename)
    if template.dtype != atomic_dtype:
        err(f"Template '{filename}' does not contain a parsed PDB")
    template_pdbs[seq] = template


conformers = {}
for seq in trinuc_sequences:
    filename = f"/home/sjoerd/data/work/ProtNAff/database/trilib/{seq}-lib-conformer.npy"
    conformer = np.load(filename)
    if conformer.dtype not in (np.float32, np.float64):
        err(f"Conformer file '{filename}' does not contain an array of numbers")
    if conformer.ndim != 3:
        err(f"Conformer file '{filename}' does not contain a 3D coordinate array")
    if conformer.shape[1] != len(template_pdbs[seq]):
        err(
            f"Sequence {seq}: conformer '{filename}' doesn't have the same number of atoms as the template"
        )
    conformers[seq] = conformer.astype(float)

results = []
for chunk in range(1, 101):
    if not os.path.exists(f"allpdb-trinuc-fit-roco-chunk-{chunk}"):
        continue
    trinuc_fitted_roco_index, trinuc_fitted_roco_data = Buffer.load(f"allpdb-trinuc-fit-roco-chunk-{chunk}").deserialize("mixed")

    trinuc_fitted_roco = {}
    codes = list(trinuc_fitted_roco_index.keys())
    for code in codes:
        pos, size = trinuc_fitted_roco_index[code]
        trinuc_fitted_roco[code] = trinuc_fitted_roco_data[pos:pos+size]

    result = trinuc_roco_grid_overlap_rmsd(
        trinuc_fitted_roco,
        template_pdbs=template_pdbs,
        trinuc_conformer_library=conformers,    
        gridspacing=np.sqrt(3)/3,
    )
    if len(result):
        results.append(result)

results = np.concatenate(results)        
np.save(f"allpdb-overlap-rmsd.npy", results)
