import os
import itertools
import numpy as np
import sys
from seamless import Buffer
from nefertiti.functions.parse_mmcif import atomic_dtype
from crocodile.trinuc.discretize_grid import discretize_grid
from crocodile.trinuc.get_overlap_rmsd import get_overlap_rmsd
import numpy as np

def err(msg):
    print(msg, file=sys.stderr)
    exit(1)

bases = ("A", "C", "G", "U")
trinuc_sequences = ["".join(s) for s in itertools.product(bases, repeat=3)]

template_pdbs = {}
for seq in trinuc_sequences:
    filename = f"templates/{seq}-template-ppdb.npy"
    template = np.load(filename)
    if template.dtype != atomic_dtype:
        err(f"Template '{filename}' does not contain a parsed PDB")
    template_pdbs[seq] = template


conformers = {}
for seq in trinuc_sequences:
    filename = f"/home/sjoerd/data/work/ProtNAff/database/trilib/{seq}-lib-conformer.npy"
    conformer = np.load(filename)
    if conformer.dtype not in (np.float32, np.float64):
        err(f"Conformer file '{filename}' does not contain an array of numbers")
    if conformer.ndim != 3:
        err(f"Conformer file '{filename}' does not contain a 3D coordinate array")
    if conformer.shape[1] != len(template_pdbs[seq]):
        err(
            f"Sequence {seq}: conformer '{filename}' doesn't have the same number of atoms as the template"
        )
    conformers[seq] = conformer.astype(float)

compatibility_sequences = ["".join(s) for s in itertools.product(("A", "C"), repeat=4)]
compatibility_matrices = {}
for compatibility_sequence in compatibility_sequences:
    compatibility_matrix = np.load(f"/home/sjoerd/data/work/ProtNAff/database/trilib/{compatibility_sequence}-compatibility-rmsd.npy")
    compatibility_matrices[compatibility_sequence] = compatibility_matrix

results = np.empty((1000000, 2))
nresults = 0
for chunk in range(1, 101):
    if not os.path.exists(f"allpdb-trinuc-fit-roco-chunk-{chunk}"):
        continue
    trinuc_fitted_roco_index, trinuc_fitted_roco_data = Buffer.load(f"allpdb-trinuc-fit-roco-chunk-{chunk}").deserialize("mixed")

    trinuc_fitted_roco = {}
    codes = list(trinuc_fitted_roco_index.keys())
    for code in codes:
        pos, size = trinuc_fitted_roco_index[code]
        trinuc_fitted_roco[code] = trinuc_fitted_roco_data[pos:pos+size]

    for code, curr_trinuc_roco in trinuc_fitted_roco.items():
        print(chunk, code, file=sys.stderr)
        trinuc_roco_grid = discretize_grid(curr_trinuc_roco, gridspacing=np.sqrt(3)/3)
        overlap_rmsd, overlap_rmsd_inds = get_overlap_rmsd(trinuc_roco_grid, template_pdbs=template_pdbs, trinuc_conformer_library=conformers, rna=True, return_indices=True)
        for overlap_r, (ind1, ind2) in zip(overlap_rmsd, overlap_rmsd_inds):
            trinuc1, trinuc2 = trinuc_roco_grid[ind1], trinuc_roco_grid[ind2]
            compat_seq = (trinuc1["sequence"] + trinuc2["sequence"][-1:]).decode().replace("U", "C").replace("G", "A")
            compat_rmsd = compatibility_matrices[compat_seq][trinuc1["conformer"],trinuc2["conformer"]]
            results[nresults] = overlap_r, compat_rmsd
            nresults += 1
results = results[:nresults]            
overlap_rmsd, compat_rmsd = results[:, 0], results[:, 1]

print("Overall:")
o = overlap_rmsd
print("Overlap RMSD < 0.75: {:.1f} %".format((o < 0.75).mean()*100))
print("Overlap RMSD < 1   : {:.1f} %".format((o < 1).mean()*100))
print("Overlap RMSD < 1.25: {:.1f} %".format((o < 1.25).mean()*100))
print()

print("Compatibility RMSD < 0.5:")
mask = (compat_rmsd<0.5)
print("{:.1f} % of the cases".format(mask.mean()*100))
o = overlap_rmsd[mask]
print("Overlap RMSD < 0.75: {:.1f} %".format((o < 0.75).mean()*100))
print("Overlap RMSD < 1   : {:.1f} %".format((o < 1).mean()*100))
print("Overlap RMSD < 1.25: {:.1f} %".format((o < 1.25).mean()*100))
print()

print("Compatibility RMSD between 0.5 and 0.75:")
mask = (compat_rmsd>0.5) & (compat_rmsd<0.75)
print("{:.1f} % of the cases".format(mask.mean()*100))
o = overlap_rmsd[mask]
print("Overlap RMSD < 0.75: {:.1f} %".format((o < 0.75).mean()*100))
print("Overlap RMSD < 1   : {:.1f} %".format((o < 1).mean()*100))
print("Overlap RMSD < 1.25: {:.1f} %".format((o < 1.25).mean()*100))
print()

print("Compatibility RMSD between 0.75 and 1:")
mask = (compat_rmsd>0.75) & (compat_rmsd<1)
print("{:.1f} % of the cases".format(mask.mean()*100))
o = overlap_rmsd[mask]
print("Overlap RMSD < 0.75: {:.1f} %".format((o < 0.75).mean()*100))
print("Overlap RMSD < 1   : {:.1f} %".format((o < 1).mean()*100))
print("Overlap RMSD < 1.25: {:.1f} %".format((o < 1.25).mean()*100))
print()

print("Compatibility RMSD between 1 and 1.25:")
mask = (compat_rmsd>1) & (compat_rmsd<1.25)
print("{:.1f} % of the cases".format(mask.mean()*100))
o = overlap_rmsd[mask]
print("Overlap RMSD < 0.75: {:.1f} %".format((o < 0.75).mean()*100))
print("Overlap RMSD < 1   : {:.1f} %".format((o < 1).mean()*100))
print("Overlap RMSD < 1.25: {:.1f} %".format((o < 1.25).mean()*100))
print()

print("Compatibility RMSD >1.25:")
mask = (compat_rmsd>1.25)
print("{:.1f} % of the cases".format(mask.mean()*100))
o = overlap_rmsd[mask]
print("Overlap RMSD < 1.25: {:.2f} %".format((o < 1.25).mean()*100))
print()