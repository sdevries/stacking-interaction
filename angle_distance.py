import numpy as np
from numpy.linalg import svd, norm

rings = {
    #nucleic acids
    "T": ["C6", "C5", "C7", "N3", "O2", "O4"],
    "U": ["N1", "C2", "N3", "C4", "C5", "C6"],
    "C": ["N1", "C2", "N3", "C4", "C5", "C6"],
    "G": ["N1", "C2", "N3", "C4", "C5", "C6", "N7", "C8", "N9"],
    "A": ["N1", "C2", "N3", "C4", "C5", "C6", "N7", "C8", "N9"], 
    
    # proteins
    "PHE": ["CG", "CD1", "CD2" , "CE1", "CE2", "CZ"],
    "TYR": ["CG", "CD1", "CD2" , "CE1", "CE2", "CZ"],
    "HIS": ["CG", "ND1", "CD2" , "CE1", "NE2"],
    "ARG": ["CD", "NE", "CZ" , "NH1", "NH2"],
    "TRP": ["CG", "CD1", "CD2" , "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"],
}

def select_rings(mol, resnames):
    if isinstance(resnames, str):
        resnames = (resnames,)
    mask = np.zeros(len(mol), bool)
    mol_ring = rings[resnames[0]]
    for resname in resnames:
        assert rings[resname] == mol_ring
        curr_mask1 = np.isin(mol["name"], [a.encode() for a in mol_ring])
        curr_mask2 = (mol["resname"] == resname.encode())
        curr_mask = curr_mask1 & curr_mask2
        s = curr_mask.sum()
        if s == 0:
            continue
        u = np.unique(mol[curr_mask]["resid"])
        nres = len(u)
        if s != nres * len(mol_ring):
            for resid in u:
                curr_mask2a = (mol["resid"] == resid)
                curr_maskx = curr_mask1 & curr_mask2a
                if curr_maskx.sum() != len(mol_ring):
                    continue
                mask |= curr_maskx
        else:                
            mask |= curr_mask
    if not mask.sum():
        return []
    result = mol[mask].reshape((-1, len(mol_ring)))
    for res in result:
        assert len(np.unique(res["resname"])) == 1, res
    return result

def calc_planes(coor):
    coor = coor - coor.mean(axis=1)[:, None]
    covar = np.einsum("ijk,ijl->ikl", coor, coor) #coor[i].T.dot(coor[i])
    v, s, wt = svd(covar)
    planes_directionless = wt[:, 2, :] / norm(wt[:, 2, :], axis=-1)[..., None]
    planes0_directed = np.cross((coor[:, 1, :] - coor[:, 0, :]), (coor[:, 2, :] - coor[:, 1, :]), axis=1)
    flip = np.sign(np.einsum("ij,ij->i", planes_directionless, planes0_directed))
    return planes_directionless * flip[:, None]


def get_coor(mol):
    mol_coor = np.stack((mol["x"], mol["y"], mol["z"]), axis=-1)
    return mol_coor


def angle_distance(struc, interfaces, prot_resnames, na_resnames):
    result = []
    for interface in interfaces:
        prot = struc[struc["chain"] == interface["chain1"]]
        na = struc[struc["chain"] == interface["chain2"]]
        
        prot_planes = []
        prot_rings_center = []
        for curr_prot_resnames in prot_resnames:
            prot_rings = select_rings(prot, curr_prot_resnames)
            if not len(prot_rings):
                continue
            prot_rings_coor = get_coor(prot_rings)
            prot_rings_coor = prot_rings_coor.dot(interface["m1"]["rotation"]) + interface["m1"]["offset"]
            curr_prot_planes = calc_planes(prot_rings_coor)
            prot_planes.append(curr_prot_planes)
            curr_prot_rings_center = prot_rings_coor.mean(axis=1)
            prot_rings_center.append(curr_prot_rings_center)
        if not len(prot_planes):
            continue
        prot_planes = np.concatenate(prot_planes)
        prot_rings_center = np.concatenate(prot_rings_center)
        
        na_planes = []
        na_rings_center = []
        for curr_na_resnames in na_resnames:
            na_rings = select_rings(na, curr_na_resnames)
            if not len(na_rings):
                continue
            na_rings_coor = get_coor(na_rings)
            na_rings_coor = na_rings_coor.dot(interface["m2"]["rotation"]) + interface["m2"]["offset"]
            curr_na_planes = calc_planes(na_rings_coor)
            na_planes.append(curr_na_planes)
            curr_na_rings_center = na_rings_coor.mean(axis=1)
            na_rings_center.append(curr_na_rings_center)

        if not len(na_planes):
            continue
        na_planes = np.concatenate(na_planes)
        na_rings_center = np.concatenate(na_rings_center)

        dis = norm((prot_rings_center[:, None]-na_rings_center[None, :]).reshape(-1, 3), axis=1)
        sin_angle = norm(np.cross(prot_planes[:, None], na_planes[None, :]).reshape(-1, 3), axis=1)

        assert dis.ndim == 1 and dis.shape == sin_angle.shape
        result.append(np.stack((dis, sin_angle),axis=-1))
    if not len(result):
        return []
    return np.concatenate(result)
