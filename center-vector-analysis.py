import numpy as np
from numpy.linalg import norm
from scipy.stats import linregress
from matplotlib import pyplot as plt

print("""Load dataset.
Single ring - single ring (PHE/TYR to U/C)
sin(ring-ring plane angle) <= 0.4
Center-center distance <= 5.3""")
centerv = np.load("center-vector-single-ring")
print("{:d} points".format(len(centerv)))
print()
center_vec = centerv[:, :3]
sin_angle = centerv[:, 3]

center_dis=norm(center_vec,axis=1)

print("Center-center distance: {:.2f} A +/- {:.3f}".format(center_dis.mean(), center_dis.std()))
print("Center-center distance, histogram")
a, b = np.histogram(center_dis)
print(list(zip(a, b[1:])))
between = (center_dis >= 3.3) & (center_dis <= 4.8)
pct = between.sum()/len(center_dis) * 100
print("Center-center distance, percentage between 3.3 A and 4.8 A: {:.1f}".format(pct))
print()

z=np.abs(center_vec[:, 2])
print("Center-center vector Z: {:.2f} A +/- {:.3f}".format(z.mean(), z.std()))
print()
print("Center-center vector Z, histogram")
a, b = np.histogram(z)
print(list(zip(a, b[1:])))
between = (z >= 2.6) & (z <= 4.1)
pct = between.sum()/len(z) * 100
print("Center-center vector Z, percentage between 2.6 A and 4.1 A: {:.1f}".format(pct))
print()

lateral=np.sqrt(center_dis**2-z**2)
print("Center-center vector XY: {:.2f} A +/- {:.3f}".format(lateral.mean(), lateral.std()))
print()


print("Linear regression between C-c distance and C-c Z")
lr=linregress(center_dis, z)
print("R = {:.3f}".format(lr.rvalue))
print()
print("Linear regression between C-c distance and sin(angle)")
lr=linregress(center_dis, sin_angle)
print("R = {:.3f}".format(lr.rvalue))
print()
print("Linear regression between C-c Z and sin(angle)")
lr=linregress(z, sin_angle)
print("R = {:.3f}".format(lr.rvalue))
print()
print("Linear regression between C-c distance and C-c XY")
lr=linregress(center_dis, lateral)
print("R = {:.3f}".format(lr.rvalue))
print("slope = {:.5f}".format(lr.slope))
print()
plt.scatter(center_dis, lateral, s=1)
plt.savefig("center-vector-analysis.png")

dis2 = center_dis - (lateral/lr.slope )
print("Corrected c-c dis (taking into account the regression above)")
print("{:.2f} A +/- {:.3f}".format(dis2.mean(), dis2.std()))
print("Corrected c-c dis, histogram")
a, b = np.histogram(dis2)
print(list(zip(a, b[1:])))
between = (dis2 >= 2.3) & (dis2 <= 3.8)
pct = between.sum()/len(dis2) * 100
print("Corrected c-c dis, percentage between 2.3 A and 3.8 A: {:.1f}".format(pct))
between = (dis2 >= 2.5) & (dis2 <= 3.4)
pct = between.sum()/len(dis2) * 100
print("Corrected c-c dis, percentage between 2.5 A and 3.4 A: {:.1f}".format(pct))