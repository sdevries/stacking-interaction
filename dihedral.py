import numpy as np
from numpy.linalg import svd, norm

RINGS = {
    #nucleic acids
    "T": ["C6", "C5", "C7", "N3", "O2", "O4"],
    "U": ["N1", "C2", "N3", "C4", "C5", "C6"],
    "C": ["N1", "C2", "N3", "C4", "C5", "C6"],
    "G": ["N1", "C2", "N3", "C4", "C5", "C6", "N7", "C8", "N9"],
    "A": ["N1", "C2", "N3", "C4", "C5", "C6", "N7", "C8", "N9"], 
    
    # proteins
    "PHE": ["CG", "CD1", "CD2" , "CE1", "CE2", "CZ"],
    "TYR": ["CG", "CD1", "CD2" , "CE1", "CE2", "CZ"],
    "HIS": ["CG", "ND1", "CD2" , "CE1", "NE2"],
    "ARG": ["CD", "NE", "CZ" , "NH1", "NH2"],
    "TRP": ["CG", "CD1", "CD2" , "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"],
}

def select_rings(mol, resnames):
    if isinstance(resnames, str):
        resnames = (resnames,)
    mask = np.zeros(len(mol), bool)
    mol_ring = RINGS[resnames[0]]
    for resname in resnames:
        assert RINGS[resname] == mol_ring
        curr_mask1 = np.isin(mol["name"], [a.encode() for a in mol_ring])
        curr_mask2 = (mol["resname"] == resname.encode())
        curr_mask = curr_mask1 & curr_mask2
        s = curr_mask.sum()
        if s == 0:
            continue
        u = np.unique(mol[curr_mask]["resid"])
        nres = len(u)
        if s != nres * len(mol_ring):
            for resid in u:
                curr_mask2a = (mol["resid"] == resid)
                curr_maskx = curr_mask1 & curr_mask2a
                if curr_maskx.sum() != len(mol_ring):
                    continue
                mask |= curr_maskx
        else:                
            mask |= curr_mask
    if not mask.sum():
        return []
    result = mol[mask].reshape((-1, len(mol_ring)))
    for res in result:
        assert len(np.unique(res["resname"])) == 1, res
    return result

def calc_planes(coor):
    coor = coor - coor.mean(axis=1)[:, None]
    covar = np.einsum("ijk,ijl->ikl", coor, coor) #coor[i].T.dot(coor[i])
    v, s, wt = svd(covar)
    planes_directionless = wt[:, 2, :] / norm(wt[:, 2, :], axis=-1)[..., None]
    planes0_directed = np.cross((coor[:, 1, :] - coor[:, 0, :]), (coor[:, 2, :] - coor[:, 1, :]), axis=1)
    flip = np.sign(np.einsum("ij,ij->i", planes_directionless, planes0_directed))
    return planes_directionless * flip[:, None]

def get_coor(mol):
    mol_coor = np.stack((mol["x"], mol["y"], mol["z"]), axis=-1)
    return mol_coor

def get_align_ring_matrices(rings):
    rings_coor = get_coor(rings)
    rings_center = rings_coor.mean(axis=1)
    planes = calc_planes(rings_coor)
    atom = rings[0][0]["name"]
    for r in rings:
        assert r[0]["name"] == atom
    vec1 = rings_coor[:, 0] - rings_center
    vec1 /= norm(vec1, axis=1)[:, None]
    vec2 = np.cross(planes, vec1)
    vec2 /= norm(vec2, axis=1)[:, None]
    vec1a = np.cross(vec2, planes)
    vec1a /= norm(vec1a, axis=1)[:, None]
    mat = np.zeros((len(rings), 3, 3))
    mat[:, :, 0] = vec1a
    mat[:, :, 1] = vec2
    mat[:, :, 2] = planes
    return rings_center, mat

def dihedral(struc, interfaces, prot_resnames, na_resnames, max_center_dis, lateral_slope, min_corrected_dis, max_corrected_dis):
    result = []
    for interface in interfaces:
        prot = struc[struc["chain"] == interface["chain1"]]
        na = struc[struc["chain"] == interface["chain2"]]
        
        prot_planes = []
        prot_rings_copy = []
        prot_rings_center = []
        for curr_prot_resnames in prot_resnames:
            prot_rings = select_rings(prot, curr_prot_resnames)
            if not len(prot_rings):
                continue
            prot_rings_coor = get_coor(prot_rings)
            prot_rings_coor = prot_rings_coor.dot(interface["m1"]["rotation"]) + interface["m1"]["offset"]
            curr_prot_planes = calc_planes(prot_rings_coor)
            prot_planes.append(curr_prot_planes)
            curr_prot_rings_center = prot_rings_coor.mean(axis=1)
            prot_rings_center.append(curr_prot_rings_center)
            
            curr_prot_rings_copy = prot_rings.copy()            
            curr_prot_rings_copy["x"] = prot_rings_coor[:, :, 0]
            curr_prot_rings_copy["y"] = prot_rings_coor[:, :, 1]
            curr_prot_rings_copy["z"] = prot_rings_coor[:, :, 2]
            prot_rings_copy.append(curr_prot_rings_copy)

        if not len(prot_planes):
            continue
        prot_planes = np.concatenate(prot_planes)
        prot_rings_copy = np.concatenate(prot_rings_copy)

        _, align_ring_matrices = get_align_ring_matrices(prot_rings_copy)
        prot_rings_center = np.concatenate(prot_rings_center)
        
        na_planes = []
        na_rings_coor = []
        for curr_na_resnames in na_resnames:
            na_rings = select_rings(na, curr_na_resnames)
            if not len(na_rings):
                continue
            curr_na_rings_coor = get_coor(na_rings)
            curr_na_rings_coor = curr_na_rings_coor.dot(interface["m2"]["rotation"]) + interface["m2"]["offset"]
            curr_na_planes = calc_planes(curr_na_rings_coor)
            na_planes.append(curr_na_planes)
            na_rings_coor.append(curr_na_rings_coor)

        if not len(na_planes):
            continue
        na_planes = np.concatenate(na_planes)
        na_rings_coor = np.concatenate(na_rings_coor)
        na_rings_center = na_rings_coor.mean(axis=1)

        sin_angle = norm(np.cross(prot_planes[:, None], na_planes[None, :]), axis=2)

        center_vec = na_rings_center[None, :]-prot_rings_center[:, None]
        #center_vec_rotated = center_vec.dot(align_ring_matrices
        
        # broadcast: center_vec_rotated[i,j] = centervec[i,j].dot(align_ring_matrices[i])
        center_vec_rotated = np.einsum("ijk,ikl->ijl", center_vec, align_ring_matrices)
        
        # broadcast: na_rings_coor_rotated[i,j] = na_rings_coor[j].dot(align_ring_matrices[i])
        na_rings_coor_rotated = np.einsum("jqk,ikl->ijql", na_rings_coor, align_ring_matrices)
        other_vec = na_rings_coor_rotated[:, :, 0, :] - na_rings_coor_rotated.mean(axis=2)
        dihed = np.angle(other_vec[:, :, 0] + 1.0j * other_vec[:, :, 1])

        center_dis = norm(center_vec_rotated, axis=2)
        assert center_dis.ndim == 2 and center_dis.shape == sin_angle.shape, (center_dis.shape, sin_angle.shape)
        
        center_vec_axial = np.abs(center_vec_rotated[:, :, 2])
        center_vec_lateral = np.sqrt(center_dis**2-center_vec_axial**2)
        center_dis_corrected = center_dis - center_vec_lateral/lateral_slope

        mask = (sin_angle < 0.4) & (center_dis < max_center_dis) & (center_dis_corrected >= min_corrected_dis) & (center_dis_corrected <= max_corrected_dis)

        if not mask.sum():
            continue
        
        result.append(dihed[mask])

    if len(result):
        result = np.concatenate(result)
    return result