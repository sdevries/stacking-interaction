import sys
import numpy as np
from tqdm import tqdm
from clusterlib import read_clustering
import nefertiti
from nefertiti.functions.superimpose import superimpose_array


diseq = sys.argv[1]
rmsd_threshold = float(sys.argv[2])
strucf = f"lib-dinuc-nonredundant-filtered-{diseq}.npy"
struc = np.load(strucf)

clustering_closest = read_clustering(f"lib-dinuc-{diseq}-{rmsd_threshold}.clust")
clustering_all = read_clustering(f"lib-dinuc-{diseq}-{rmsd_threshold}.all.clust")

clusters = [c[0]-1 for c in clustering_closest]
assert sorted(clusters) == sorted([c[0]-1 for c in clustering_all])

clustering_closest = {c[0]-1:[cc-1 for cc in c] for c in clustering_closest}
clustering_all = {c[0]-1:[cc-1 for cc in c] for c in clustering_all}

cluster_struc = struc[clusters]

# Verify that the clusters are at least the minimum distance apart
for clusnr, clus in tqdm(enumerate(cluster_struc), total=len(clusters)):
    _, rmsd = superimpose_array(cluster_struc, clus)
    rmsd[clusnr] = np.inf
    assert rmsd.min() + 0.01 > rmsd_threshold, (rmsd.min(), clusters[clusnr], clusters[rmsd.argmin()])

# Select structures at random
NSAMPLE = 1000
np.random.seed(0)
sample_struc_ind = np.random.choice(len(struc), NSAMPLE, replace=True)
sample_struc_ind = np.unique(sample_struc_ind)

sample_struc_set = set(sample_struc_ind)
# Find all clusters that contain at least one of the sample structures
sample_clusters = []
for clus, c in clustering_all.items():
    if sample_struc_set.intersection(c):
        sample_clusters.append(clus)

sample_cluster_struc = struc[sample_clusters]
for ind in tqdm(sample_struc_ind):
    s = struc[ind]
    is_close = []
    closest = None
    for clusnr, clus in enumerate(sample_clusters):
        is_close.append(ind in clustering_all[clus])
        if ind in clustering_closest[clus]:
            closest = clusnr
    assert closest is not None, ind
    _, rmsd = superimpose_array(sample_cluster_struc, s)

    if rmsd.argmin() != closest:
        print(f"Structure {ind}: incorrect closest cluster", file=sys.stderr)
    for n, clus in enumerate(sample_clusters):
        if is_close[n]:
            if rmsd[n] > rmsd_threshold + 0.1:
                print(f"Structure {ind}: assigned to cluster {clus}, which is not close", file=sys.stderr)
        else:
            if rmsd[n] < rmsd_threshold - 0.1:
                print(f"Structure {ind}: not assigned to cluster {clus}, but it is close", file=sys.stderr)