import numpy as np
from numpy.linalg import norm
from scipy.spatial.transform import Rotation
from matplotlib import pyplot as plt
import matplotlib.pyplot as plt
import seaborn as sns

print("""Load dataset.
Single ring - single ring (PHE/TYR to U/C)
Corrected center-center distance <= 5.0""")
centerv = np.load("center-vector2-single-ring")
print("{:d} points".format(len(centerv)))
center_vectors, corrected_dis, sin_angle = centerv[:, :3], centerv[:, 3], centerv[:, 4]
center_dis = norm(center_vectors, axis=1)
mask = (center_dis < 5.3)
center_vectors = center_vectors[mask]
corrected_dis = corrected_dis[mask]
sin_angle = sin_angle[mask]
center_dis = center_dis[mask]

np.random.seed(0)
r1 = Rotation.random(200).as_matrix()[:, 2]
r2 = Rotation.random(200).as_matrix()[:, 2]
random_cross = np.cross(r1[:, None, :], r2[None, :, :])
random_cross = random_cross[np.triu_indices(len(r1))]
random_sin_ang = norm(random_cross, axis=1)

upper_limits = np.arange(2,4,0.1).tolist() +  np.arange(4,5,0.2).tolist()
lower_limit = 0
labels = []
sections = {}
density = {}
for upper_limit in upper_limits:
    mask = ((corrected_dis>lower_limit) & (corrected_dis<=upper_limit))
    npoints = mask.sum()
    if npoints < 100:
        continue
    print("{:.1f} {:.1f} {}".format(lower_limit, upper_limit, npoints))
    x = (lower_limit+upper_limit)/2
    sections[x] = sin_angle[mask]
    density[x] = npoints/(upper_limit-lower_limit)
    lower_limit = upper_limit
    labels.append("{:.1f}".format(upper_limit))

sections[6] = random_sin_ang
labels.append("Random")

fig, ax = plt.subplots(figsize=(50,12))

max_density = max(density.values())
for k,v in density.items():
    density[k] = v / max_density
sns.barplot(x=density.keys(),y=density.values(),ax=ax, color="grey", alpha=0.2, width=1)    

sns.violinplot(sections,cut=0, ax=ax)
for label in ax.get_yticklabels():
    label.set_fontsize(35)
ax.set_xlabel("Filtered (< 5.3 A) and corrected center-center distance (A)", fontsize=35)
ax.set_ylabel("sin(ring-ring plane angle)", fontsize=35, labelpad=30)
ax.xaxis.set_tick_params(rotation=70)
ax.set_xticklabels(labels, size=35)
fig.tight_layout()
fig.savefig("plot-angle-corrected-vector.png")

center_z = np.abs(center_vectors[:, 2])
upper_limits = np.arange(2.3,4.3,0.1).tolist() +  np.arange(4.3,5.3,0.2).tolist()
lower_limit = 0
labels = []
sections = {}
density = {}
for upper_limit in upper_limits:
    mask = ((center_z>lower_limit) & (center_z<=upper_limit))
    npoints = mask.sum()
    if npoints < 100:
        continue
    print("{:.1f} {:.1f} {}".format(lower_limit, upper_limit, npoints))
    x = (lower_limit+upper_limit)/2
    sections[x] = sin_angle[mask]
    density[x] = npoints/(upper_limit-lower_limit)
    lower_limit = upper_limit
    labels.append("{:.1f}".format(upper_limit))

sections[6] = random_sin_ang
labels.append("Random")

fig, ax = plt.subplots(figsize=(50,12))

max_density = max(density.values())
for k,v in density.items():
    density[k] = v / max_density
sns.barplot(x=density.keys(),y=density.values(),ax=ax, color="grey", alpha=0.2, width=1)    

sns.violinplot(sections,cut=0, ax=ax)
for label in ax.get_yticklabels():
    label.set_fontsize(35)
ax.set_xlabel("Filtered (< 5.3 A) and center-center Z distance (A)", fontsize=35)
ax.set_ylabel("sin(ring-ring plane angle)", fontsize=35, labelpad=30)
ax.xaxis.set_tick_params(rotation=70)
ax.set_xticklabels(labels, size=35)
fig.tight_layout()
fig.savefig("plot-angle-z-vector.png")

print("Filtering: center-center distance <= 5.3 A")
print("Total number of sin(ring-ring plane) < 0.4: {:d}".format((sin_angle < 0.4).sum()))
print()

print("Filtering: center-center distance <= 5.0 A")
print("  and center-center Z distance between 2.3 and 4.5 A")
print("Post-filtering sin(ring-ring plane angle) histogram")
mask = (center_z >= 2.3) & (center_z <= 4.5) & (center_dis < 5.0)
print("Under 0.4: {:d}, {:.1f} percent".format((sin_angle[mask] < 0.4).sum(), (sin_angle[mask] < 0.4).sum()/mask.sum() * 100))
tp = (sin_angle[mask] < 0.4).sum()
p = (sin_angle < 0.4).sum()
print("Coverage under 0.4: {:d}/{:d}, {:.1f} percent".format(tp, p, tp/p*100))
print()

print("Filtering: center-center distance <= 5.0 A")
print("  and center-center Z distance between 2.3 and 4.5 A")
print("  and corrected center-center distance between 2.3 and 3.8 A")
print("Post-filtering sin(ring-ring plane angle) histogram")
mask = (corrected_dis >= 2.3) & (corrected_dis <= 3.8) & (center_z >= 2.3) & (center_z <= 4.5) & (center_dis < 5.0)
a, b = np.histogram(sin_angle[mask])
print(list(zip(a, b[1:])))
print("Under 0.4: {:d}, {:.1f} percent".format((sin_angle[mask] < 0.4).sum(), (sin_angle[mask] < 0.4).sum()/mask.sum() * 100))
tp = (sin_angle[mask] < 0.4).sum()
p = (sin_angle < 0.4).sum()
print("Coverage under 0.4: {:d}/{:d}, {:.1f} percent".format(tp, p, tp/p*100))
tp = (sin_angle[mask] < np.sin(15/180*np.pi)).sum()
print("Coverage if filtering angle at 15 degrees: {:d}/{:d}, {:.1f} percent".format(tp, p, tp/p*100))
print()
mask = (corrected_dis >= 2.5) & (corrected_dis <= 3.4) & (center_z >= 2.3) & (center_z <= 4.5) & (center_dis < 4.6)
print("Filtering: center-center distance <= 4.6 A")
print("  and center-center Z distance between 2.3 and 4.5 A")
print("  and corrected center-center distance between 2.5 and 3.4 A")
print("Post-filtering sin(ring-ring plane angle) histogram")
a, b = np.histogram(sin_angle[mask])
print(list(zip(a, b[1:])))
print("Under 0.4: {:d}, {:.1f} percent".format((sin_angle[mask] < 0.4).sum(), (sin_angle[mask] < 0.4).sum()/mask.sum() * 100))
print("Under 0.5: {:d}, {:.1f} percent".format((sin_angle[mask] < 0.5).sum(), (sin_angle[mask] < 0.5).sum()/mask.sum() * 100))
print()
print("Rotational space: distribution of sin(ring-ring plane angle) for random planes")
print("Under 0.4: {:.1f} percent".format((random_sin_ang < 0.4).sum()/len(random_sin_ang) * 100))
print("Under 0.5: {:.1f} percent".format((random_sin_ang < 0.5).sum()/len(random_sin_ang) * 100))
print()
print("Translational space")
print("Define a grid of 1/3*sqrt(3) Angstroms spacing.")
print("This will give a maximum distance from the center of 0.5 A")
print(" and a mean square distance from the center of sqrt(a**2/4) = 0.29 A")
print("Filtering: center-center distance <= 5.0 A")
print("  and corrected center-center distance between 2.3 and 3.8 A")
lateral_slope = 1.78966
a = 1.0/3 * np.sqrt(3)
max_dist = 5   # max center-center distance
max_steps = np.floor(max_dist / a)
print(max_steps, a)
space = np.arange(-max_steps, max_steps+1) * a
grid = np.stack(np.meshgrid(space, space, space, indexing='ij'), axis=-1)
grid_center_dis = norm(grid, axis=3)
grid_lateral = np.sqrt(grid_center_dis**2 - grid[:, :, :, 2]**2) # or: norm(grid[:, :, :, :2], axis=3)
grid_center_dis_corrected = grid_center_dis - grid_lateral / lateral_slope
mask1 = (grid_center_dis < 5)  
mask2 = (grid_center_dis_corrected >= 2.3) & (grid_center_dis_corrected <= 3.8)
mask = mask1 & mask2
print("Number of grid points: {:d}".format(mask.sum()))