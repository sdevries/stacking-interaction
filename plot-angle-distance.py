import numpy as np
from numpy.linalg import norm
from scipy.spatial.transform import Rotation
from seamless import Buffer
import matplotlib.pyplot as plt
import seaborn as sns

dis_angle = Buffer.load("angle-distance-single-ring").deserialize(celltype="binary")
dis = dis_angle[:, 0]
sin_angle = dis_angle[:, 1]

lower_limit = 0
sections = {}

r1 = Rotation.random(200).as_matrix()[:, 2]
r2 = Rotation.random(200).as_matrix()[:, 2]
random_cross = np.cross(r1[:, None, :], r2[None, :, :])
random_cross = random_cross[np.triu_indices(len(r1))]
random_sin_ang = norm(random_cross, axis=1)

upper_limits = np.arange(1,6,0.1).tolist() +  np.arange(6,10,0.5).tolist() +  np.arange(10,21,2).tolist()
labels = []
for upper_limit in upper_limits:
    mask = ((dis>lower_limit) & (dis<=upper_limit))
    if mask.sum() < 100:
        continue
    print("{:.1f} {:.1f} {}".format(lower_limit, upper_limit, mask.sum()))
    sections[(lower_limit+upper_limit)/2] = sin_angle[mask]
    lower_limit = upper_limit
    labels.append("{:.1f}".format(upper_limit))

sections[21] = random_sin_ang
labels.append("Random")

fig, ax = plt.subplots(figsize=(50,12))

sns.violinplot(sections,cut=0, ax=ax)
for label in ax.get_yticklabels():
    label.set_fontsize(35)
ax.set_xlabel("Center-center distance (A)", fontsize=35)
ax.set_ylabel("sin(ring-ring plane angle)", fontsize=35, labelpad=30)
ax.xaxis.set_tick_params(rotation=70)
ax.set_xticklabels(labels, size=35)
fig.tight_layout()
fig.savefig("plot-angle-distance.png")

print()
v = ((dis < 4.0) & (sin_angle <= 0.4)).sum()
print("Ring-ring distance under 4.0 A, sin(angle) <= 0.4", v)
v = ((dis < 4.0) & (sin_angle > 0.4)).sum()
print("Ring-ring distance under 4.0 A, sin(angle) > 0.4", v)
v = ((dis > 4.0) & (dis < 5.3) & (sin_angle <= 0.4)).sum()
print("Ring-ring distance 4.0 - 5.3 A, sin(angle) <= 0.4", v)
v = ((dis > 4.0) & (dis < 5.3) & (sin_angle > 0.4)).sum()
print("Ring-ring distance 4.0 - 5.3 A, sin(angle) > 0.4", v)
print()
v = ((dis < 4.0) & (sin_angle <= 0.3)).sum()
print("Ring-ring distance under 4.0 A, sin(angle) <= 0.3", v)
v = ((dis < 4.0) & (sin_angle > 0.3)).sum()
print("Ring-ring distance under 4.0 A, sin(angle) > 0.3", v)
v = ((dis > 4.0) & (dis < 5.3) & (sin_angle <= 0.3)).sum()
print("Ring-ring distance 4.0 - 5.3 A, sin(angle) <= 0.3", v)
v = ((dis > 4.0) & (dis < 5.3) & (sin_angle > 0.3)).sum()
print("Ring-ring distance 4.0 - 5.3 A, sin(angle) > 0.3", v)
