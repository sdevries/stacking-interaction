import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
dihedrals=np.load("dihedrals-single-ring")
sns.histplot(dihedrals/np.pi*180,bins=np.arange(-180,180+5,5))
plt.savefig("plot-dihedrals.png")
mask = (dihedrals <= -0.25 * np.pi) | (dihedrals >= 0.25 * np.pi)
print("Dihedrals <= -45 degrees or >= 45 degrees: {:d} ( {:.1f} % )".format(mask.sum(), mask.sum()/len(dihedrals) * 100))