import os
import sys
import numpy as np
import pandas as pd
import itertools
import seaborn as sns
from matplotlib import pyplot as plt

lib = sys.argv[1]
assert lib in ("dinuc", "trinuc")

refit_file = sys.argv[2]  # context-specific closest fit file
outfile = sys.argv[3]

fig, ax = plt.subplots()

data = pd.DataFrame()
bins = [0] + np.arange(0.2, 3.1, 0.05).tolist()
data.index = bins


def dat(d1, d2):
    d = d1
    if d2 is not None:
        d = np.concatenate([d1, d2])
    result = []
    for bin in bins:
        fail = (d >= bin).sum() / len(d)
        result.append(fail)
    return result


n_nuc = 2 if lib == "dinuc" else 3
motifs = ["".join(c) for c in itertools.product("AC", repeat=n_nuc)]

baseline = []
for motif in motifs:
    motif_baseline = np.loadtxt(f"lib-{lib}-{motif}-closest-fit.txt")[:, 1]
    baseline.append(motif_baseline)
baseline = np.concatenate(baseline)
baseline = dat(baseline, None)
data["baseline"] = baseline

data_refit = np.loadtxt(refit_file)[:, 1]
data_refit = dat(data_refit, None)
data[refit_file] = data_refit
sns.lineplot(data, ax=ax)

ax.set_ybound(0, 1)
print("Generated", outfile)
fig.savefig(outfile)

ax.set_yscale("log")
ax.set_ybound(0.00001, 1)
log_outf = os.path.splitext(outfile)[0] + "-log.png"
print("Generated", log_outf)
fig.savefig(log_outf)
