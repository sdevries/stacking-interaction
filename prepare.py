import os
from parse_mmcif import parse_mmcif
from parse_mmcif_header import parse_mmcif_header
from detect_interfaces import detect_interfaces
import numpy as np
import glob
import json

filelist = glob.glob("data/*.cif")
for f in filelist:
    print(f)
    ff = os.path.splitext(f)[0]
    with open(f) as fp:
        data = fp.read()
    struc = parse_mmcif(data, auth_chains=False, auth_residues=False)
    np.save(ff + ".npy", struc)
    header = parse_mmcif_header(data)
    with open(ff + ".json", "w") as fp:
        json.dump(header, fp, indent=2, sort_keys=True)
    interfaces = detect_interfaces(struc, header)
    np.save(ff + "-interface.npy", interfaces)
    print(f)