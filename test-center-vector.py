
import sys
import numpy as np
import center_vector
from nefertiti.functions.write_pdb import write_pdb
from seamless import Buffer

prot_resnames = (("PHE", "TYR"),)
na_resnames = (("U", "C"),)

# example usage: test-center-vector.py 1b7f 5.3 0.4
code = sys.argv[1]
max_dis = float(sys.argv[2])
max_sin_angle = float(sys.argv[3])

struc = np.load("data/{}.npy".format(code))
struc = struc[struc["model"] == struc[0]["model"]]
interfaces_index, interfaces_data = Buffer.load("allpdb-interfaces").deserialize("mixed")
code2=code+".cif"
interfaces = interfaces_data[interfaces_index[code2][0]:interfaces_index[code2][0]+interfaces_index[code2][1]]

center_vecs = center_vector.center_vector(struc, interfaces, prot_resnames, na_resnames, max_dis, max_sin_angle)

for ifnr, interface in enumerate(interfaces):
    ringring, vec = center_vecs[ifnr]

    prot = struc[struc["chain"] == interface["chain1"]].copy()
    na = struc[struc["chain"] == interface["chain2"]].copy()
    prot_coors0 = center_vector.get_coor(prot)    
    prot_coors = prot_coors0.dot(interface["m1"]["rotation"]) + interface["m1"]["offset"]
    prot["x"], prot["y"], prot["z"] = prot_coors.T
    na_coors0 = center_vector.get_coor(na)
    na_coors = na_coors0.dot(interface["m2"]["rotation"]) + interface["m2"]["offset"]
    na["x"], na["y"], na["z"] = na_coors.T

    prot_rings = center_vector.select_rings(prot, prot_resnames[0])
    na_rings = center_vector.select_rings(na, na_resnames[0])
    align_ring_matrices = center_vector.get_align_ring_matrices(prot_rings)
    offsets, rotmats = align_ring_matrices
    for n in range(len(ringring)):
        i, ii = ringring[n]

        prot_ring = prot_rings[i]
        offset, rotmat = align_ring_matrices[0][i], align_ring_matrices[1][i]
        new_mol = prot_ring.copy()
        new_coor = center_vector.get_coor(new_mol)
        new_coor = (new_coor - offset).dot(rotmat)
        new_mol["x"], new_mol["y"], new_mol["z"] = new_coor.T

        na_ring = na_rings[ii]
        new_mol2 = na_ring.copy()
        new_coor2 = center_vector.get_coor(new_mol2)
        new_coor2 = (new_coor2 - offset).dot(rotmat)
        new_mol2["x"], new_mol2["y"], new_mol2["z"] = new_coor2.T

        pdb = write_pdb(np.concatenate((new_mol, new_mol2), dtype=prot_ring.dtype))
        with open("ring-{}-{}-{}.pdb".format(ifnr+1, i+1, ii+1), "w") as f:
            f.write(pdb)


        new_mol = np.concatenate((prot, na), dtype=prot.dtype)
        new_coor = center_vector.get_coor(new_mol)
        new_coor = (new_coor - offset).dot(rotmat)
        new_mol["x"], new_mol["y"], new_mol["z"] = new_coor.T
        pdb = write_pdb(new_mol)
        with open("interface-{}-{}-{}.pdb".format(ifnr+1, i+1, ii+1), "w") as f:
            f.write(pdb)
