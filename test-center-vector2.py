
import sys
import numpy as np
import center_vector2
from nefertiti.functions.write_pdb import write_pdb
from seamless import Buffer

prot_resnames = (("PHE", "TYR"),)
na_resnames = (("U", "C"),)

# example usage: test-center-vector2.py 1b7f
code = sys.argv[1]

struc = np.load("data/{}.npy".format(code))
struc = struc[struc["model"] == struc[0]["model"]]
interfaces_index, interfaces_data = Buffer.load("allpdb-interfaces").deserialize("mixed")
code2=code+".cif"
interfaces = interfaces_data[interfaces_index[code2][0]:interfaces_index[code2][0]+interfaces_index[code2][1]]

lateral_slope = 1.78966
center_vecs = center_vector2.center_vector2(
    struc, interfaces, prot_resnames, na_resnames, 
    lateral_slope=lateral_slope, 
    min_corrected_dis=0,
    max_corrected_dis=5
)
center_vectors, corrected_dis, sin_ang = center_vecs[:, :3], center_vecs[:, 3], center_vecs[:, 4]
print(center_vectors)
print(corrected_dis)
print(sin_ang)