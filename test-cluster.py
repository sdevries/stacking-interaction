import numpy as np
from clusterlib import cluster, cluster_from_pairs
from nefertiti.functions.superimpose import superimpose_array

coorf = f"lib-trinuc-nonredundant-filtered-AAA.npy"
NSAMPLE = 1000

coor = np.load(coorf)
centered_coor = coor - coor.mean(axis=1)[:, None, :]

np.random.seed(0)
sample_ind = np.random.choice(min(len(coor), 10000), NSAMPLE, replace=False)
sample = centered_coor[sample_ind]
rmsdmat = np.zeros(
    (NSAMPLE, NSAMPLE),
)
for n in range(NSAMPLE):
    _, rmsd = superimpose_array(sample, sample[n])
    rmsdmat[n] = rmsd

nb_mat = rmsdmat < 1
print("start")
clustering = cluster(nb_mat)
print(len(clustering))
print()

nb_pairs = np.stack(np.nonzero(nb_mat), axis=1)
clustering2 = cluster_from_pairs(nb_pairs, NSAMPLE)
print(len(clustering2))
print()
print("Identical:", clustering == clustering2)

import seamless

seamless.delegate(False)
from clusterlib.seamless_api import peel_cluster


print([c[0] for c in clustering])
clusters = peel_cluster(
    sample,
    1,
    NSAMPLES=[50, 100, 200, 400, 400, 800, 800, 1600, 1600, 3200],
    MAX_UNCLUSTERED=int(len(sample) * 1 / 3 + 0.999),
    RANDOM_SEED=0,
    SPECIAL__REPORT_PROGRESS=True,
)
print("Peel    :", clusters)
print("Non-peel:", [c[0] for c in clustering][: len(clusters)])
print("     len:", [len(c) for c in clustering][: len(clusters)])
