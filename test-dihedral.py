
import sys
import numpy as np
import dihedral
from nefertiti.functions.write_pdb import write_pdb
from seamless import Buffer

prot_resnames = (("PHE", "TYR"),)
na_resnames = (("U", "C"),)

# example usage: test-dihedral.py 1b7f
code = sys.argv[1]

struc = np.load("data/{}.npy".format(code))
struc = struc[struc["model"] == struc[0]["model"]]
interfaces_index, interfaces_data = Buffer.load("allpdb-interfaces").deserialize("mixed")
code2=code+".cif"
interfaces = interfaces_data[interfaces_index[code2][0]:interfaces_index[code2][0]+interfaces_index[code2][1]]

lateral_slope = 1.78966
dihedrals = dihedral.dihedral(
    struc, interfaces, prot_resnames, na_resnames, 
    max_center_dis=5.0,
    lateral_slope=lateral_slope,
    min_corrected_dis=2.3,
    max_corrected_dis=3.8
)
print(dihedrals)
