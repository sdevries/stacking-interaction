set -u -e
currdir=`python3 -c 'import os,sys;print(os.path.dirname(os.path.realpath(sys.argv[1])))' $0`
cd $currdir
seamless-delegate-stop
export SEAMLESS_HASHSERVER_DIRECTORY=$(realpath ./buffers)
export SEAMLESS_DATABASE_DIRECTORY=$(realpath ./database)
rm -rf $SEAMLESS_HASHSERVER_DIRECTORY $SEAMLESS_DATABASE_DIRECTORY
mkdir $SEAMLESS_HASHSERVER_DIRECTORY $SEAMLESS_DATABASE_DIRECTORY
seamless-delegate none
cd ..
rm -rf data.CHECKSUM
seamless-upload -y data