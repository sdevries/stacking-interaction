import numpy as np

def trinuc_fit_roco(
    trinuc_fitted: dict[str, np.ndarray],
    strucs: dict[str, np.ndarray],
    *,
    template_pdbs: dict[str, np.ndarray],
    trinuc_conformer_library: dict[str, np.ndarray],
    trinuc_rotaconformer_library: dict[(str, int), str],
    rmsd_margin2: float,
    conformer_rmsd_min: float,
    conformer_rmsd_max: float
):
    # TODO: include crocodile/nefertiti as Seamless modules
    from crocodile.trinuc.fit_roco import fit_roco
    from crocodile.trinuc.best_fit import best_fit

    assert len(strucs.keys()) == len(trinuc_fitted.keys())
    result_list = fit_roco(
        list(trinuc_fitted.values()),
        [strucs[code] for code in trinuc_fitted],
        template_pdbs=template_pdbs,
        trinuc_conformer_library=trinuc_conformer_library,
        trinuc_rotaconformer_library=trinuc_rotaconformer_library,
        rna=True,
        prefilter_rmsd_min=conformer_rmsd_min,
        prefilter_rmsd_max=conformer_rmsd_max,
        rmsd_margin=rmsd_margin2
    )
    result = {}
    for n, code in enumerate(trinuc_fitted.keys()):
        d = result_list[n]
        if len(d):
            result[code] = result_list[n]

    for code, trinuc_roco in result.items():
        bf = best_fit(trinuc_roco)
        print(code, len(bf), bf["rmsd"] + 0.001)
    print()

    result_data = []
    result_index = {}
    offset = 0
    for code, trinuc_roco in result.items():
        result_data.append(trinuc_roco)
        result_index[code] = (offset, len(trinuc_roco))
        offset += len(trinuc_roco)
    result_data = np.concatenate(result_data)
    return result_index, result_data
