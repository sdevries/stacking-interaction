import numpy as np

def trinuc_roco_grid_overlap_rmsd(
    trinuc_fitted_roco: dict[str, np.ndarray],
    *,
    template_pdbs: dict[str, np.ndarray],
    trinuc_conformer_library: dict[str, np.ndarray],
    gridspacing: float,        
):
    from crocodile.trinuc.discretize_grid import discretize_grid
    from crocodile.trinuc.get_overlap_rmsd import get_overlap_rmsd
    result0 = []
    for code, curr_trinuc_roco in trinuc_fitted_roco.items():
        trinuc_roco_grid = discretize_grid(curr_trinuc_roco, gridspacing=gridspacing)
        print(code, len(trinuc_roco_grid), trinuc_roco_grid["rmsd"] + 0.001)
        overlap_rmsd = get_overlap_rmsd(trinuc_roco_grid, template_pdbs=template_pdbs, trinuc_conformer_library=trinuc_conformer_library, rna=True)
        print(code, len(overlap_rmsd), overlap_rmsd)
        result0.append(overlap_rmsd)
    result0 = np.concatenate(result0)
    result_dtype = np.dtype(
        [
            ("conformer1", np.uint16),
            ("conformer2", np.uint16),
            ("fit1", np.float32),
            ("fit2", np.float32),
            ("overlap_rmsd", np.float32),
        ]
    )
    result = np.empty(len(result0), dtype=result_dtype)
    for field in result_dtype.fields:
        result[field] = result0[field]

    return result
